﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridManager : MonoBehaviour
{
    private int rows = 10;
    private int cols = 10;
    public float tileSize = 2.5f;
    public GameObject prefabCase;
    
    
    
    // Start is called before the first frame update
    void Start()
    {
        //GenerateGrid();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void GenerateGrid()
    {
        for (int row = 0; row < rows; row++)
        {
            for (int col = 0; col < cols; col++)
            {
                GameObject tile = (GameObject) Instantiate(prefabCase, transform);
                float posX = col * tileSize;
                float posY = row * -tileSize;
                
                tile.transform.position = new Vector2(posX,posY);
            }
        }
        float gridW = cols * tileSize;
        float gridH = rows * tileSize;
        transform.position = new Vector2(-gridW / 2 + tileSize / 2, gridH /2 - tileSize / 2);
    }


}
