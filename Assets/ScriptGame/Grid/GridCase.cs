﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridCase : MonoBehaviour
{
    public GameplayManager gameplayManager;
    public bool isUsed;

    // Start is called before the first frame update
    void Start()
    {
        gameplayManager = FindObjectOfType<GameplayManager>();
        /*
        if(gameObject.name == "Case(Clone)")
            Debug.Log(transform.position);
            */
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnMouseDown()
    {
        if (!isUsed)
        {
            //Debug.Log(transform.position);
            //Player 1
            if (gameObject.layer == 10)
            {
                if (gameplayManager.player1CanAttack)
                    StartCoroutine(gameplayManager.State.DisplayAttackPlayer1(gameObject));
                //gameplayManager.AttackAtPlayer2(transform.position);
            }
            //Player 2
            else if (gameObject.layer == 11)
            {
                if(gameplayManager.player2CanAttack)
                    StartCoroutine(gameplayManager.State.DisplayAttackPlayer2(gameObject));
                //gameplayManager.AttackAtPlayer1(transform.position);

            }
        }
    }

    /*
    private void OnMouseOver()
    {
        GetComponent<SpriteRenderer>().color = Color.gray;
    }

    private void OnMouseExit()
    {
        GetComponent<SpriteRenderer>().color = Color.white;
    }
    */
}
