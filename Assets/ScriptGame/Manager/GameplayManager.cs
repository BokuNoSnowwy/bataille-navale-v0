﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameplayManager : StateMachine
{
    public TextMeshProUGUI textInfo;
    public GameObject replayButton;
    
    public GameObject Player1Objects;
    public GameObject Player2Objects;
    
    public GameObject Player1ValidateButton;
    public GameObject Player2ValidateButton;

    public bool player1CanAttack;
    public bool player2CanAttack;
    
    //Placement de bateaux
    public int shipPlacedP1;
    public int shipPlacedP2;

    public List<GameObject> listOfShipP1 = new List<GameObject>();
    public List<GameObject> listOfShipP2 = new List<GameObject>();
    
    public int testValue;

    // Start is called before the first frame update
    void Start()
    {
        SetState(new StartGame(this));
    }

    // Update is called once per frame
    void Update()
    {

    }

    //STARTGAME PHASE
    
    //Vérifie 
    public void OnValidatePlacementPlayer1()
    {
        Debug.Log("Validate Player1");
        foreach (var ship in FindObjectsOfType<ShipManagerVM>())
        {
            listOfShipP1.Add(ship.gameObject);
            if (ship.listCase.Count == ship.ship.shipSize)
            {
                shipPlacedP1++;
            }
        }
        StartCoroutine(State.ShipPlacementPlayer2());
    }
    public void OnValidatePlacementPlayer2()
    {
        Debug.Log("Validate Player2");
        foreach (var ship in GameObject.FindObjectsOfType<ShipManagerVM>())
        {
            listOfShipP2.Add(ship.gameObject);
            if (ship.listCase.Count == ship.ship.shipSize)
            {
                shipPlacedP2++;
            }
        }
        StartCoroutine(State.PutShipsOnBattlePhase());
    }

    //BATTLE PHASE
    public void StartCoroutinePlayer1AttackTurn()
    {
        StartCoroutine(State.AttackPlayer1());
    }
    
    public void StartCoroutinePlayer2AttackTurn()
    {
        StartCoroutine(State.AttackPlayer2());
    }
    
    public void DisplayElementP2()
    {
        Player1Objects.SetActive(false);
        Player2Objects.SetActive(true);
        
        Player1ValidateButton.SetActive(false);
        Player2ValidateButton.SetActive(false);
    }

    public void DisplayElementP1()
    {
        Player1Objects.SetActive(true);
        Player2Objects.SetActive(false);
        
        Player1ValidateButton.SetActive(false);
        Player2ValidateButton.SetActive(false);
    }

    public void CheckVictory()
    {
        Debug.Log("CheckVictory");
        if (shipPlacedP1 <= 0)
        {
            SetState(new VictoryPhase(this));
            StartCoroutine(State.VictoryPlayer2());
        }
        else if(shipPlacedP2 <= 0)
        {
            SetState(new VictoryPhase(this));
            StartCoroutine(State.VictoryPlayer1());
        }
    }

    public void Replay()
    {
        SceneManager.LoadScene(0);
    }
}
