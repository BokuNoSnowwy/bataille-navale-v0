﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartShipDetection : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<ShipManagerVM>())
        {
            if (name != transform.parent.gameObject.transform.parent.GetComponent<ShipManagerVM>().name)
            {
                Debug.Log(transform.parent.gameObject.transform.parent.GetComponent<ShipManagerVM>().name);
                transform.parent.GetComponent<CubeDetection>().isNearOtherShip = true;
            }
        }
        else
        {
            transform.parent.GetComponent<CubeDetection>().isNearOtherShip = false;
        }
    }
}
