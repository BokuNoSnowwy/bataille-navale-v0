﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipManagerVM : MonoBehaviour
{
    public float distance = 10f;
    public float intRotate = 90;
    public Ship ship;
    public List<GameObject> listCase = new List<GameObject>();
    public Transform yourShip;
    public Vector3 startPos;
    
    public GameplayManager gameplayManager;
    
    public bool onBattlePhase;
    public bool canBeMoved;
    public bool isSelected;
    public List<bool> isChecked = new List<bool>();
    // Start is called before the first frame update
    void Start()
    {
        //Permet de réinitialliser la position quand il a mit un bateau trop proche d'un autre
        startPos = transform.position;
        
        for (int i = 0; i < ship.shipSize; i++)
        {
            Instantiate(ship.shipCube,new Vector3(yourShip.position.x+0.55f * i,yourShip.position.y,yourShip.position.z +1f),Quaternion.identity,yourShip);
        }
        
        gameplayManager = FindObjectOfType<GameplayManager>();
        
        foreach (Transform child in transform)
        {
            //Debug.Log(child.name +" " + child.position);
            listCase.Add(child.gameObject);
        }
        
        StartPhase();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(1) && isSelected)
        {
            transform.Rotate(new Vector3(0,0,1),intRotate);
        }
        isSelected = false;
    }
    
    //
    private void OnMouseDrag()
    {
        if (canBeMoved && !onBattlePhase)
        {
            isSelected = true;
            Vector3 mousePos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, distance);
            Vector3 objPos = Camera.main.ScreenToWorldPoint(mousePos);
            transform.position = objPos;
        }
    }
    
    //QUand on relache la souris après avoir grab un bateau, il y a un check pour savoir si il peut se poser la, si il peut, le place, sinon le remet sur sa position initiale
    private void OnMouseUp()
    {
        foreach (var shipPart in listCase)
        {
            if (shipPart.GetComponent<CubeDetection>().isNearOtherShip)
            {
                Debug.Log("Impossible de mettre un bateau ici");
                transform.position = startPos;
            }
        }
        
        if (CheckShipParts())
        {
            transform.position = new Vector3(transform.GetChild(0).GetComponent<CubeDetection>().caseObject.transform.position.x,transform.GetChild(0).GetComponent<CubeDetection>().caseObject.transform.position.y, 0);
            gameplayManager.testValue++;
        }
        else
        {
            transform.position = startPos;
        }
    }
    
    public void BattlePhase()
    {
        Debug.Log("OnBattlePhase" + gameObject.name);
        onBattlePhase = true;
        canBeMoved = false;
    }

    public void StartPhase()
    {
        onBattlePhase = false;
        canBeMoved = true;
    }

    //Check le nombre de cases restante sur le bateau pour savoir si il est coulé ou non 
    public void CheckRemainingCase()
    {

        int nbOfCase = 0;
        foreach (var aCase in listCase)
        {
            if (aCase.GetComponent<CubeDetection>().isTouched)
            {
                nbOfCase += 1;
            }   
        }
        Debug.Log(nbOfCase);
        if (nbOfCase == ship.shipSize && onBattlePhase)
        {
            if (ship.Player == 1)
            {
                Debug.Log("Ship destroyed player 1");
                gameplayManager.shipPlacedP1--;
            }
            else if(ship.Player == 2)
            {
                Debug.Log("Ship destroyed player 2");
                gameplayManager.shipPlacedP2--;
            }
        }
    }

    //Test pour savoir si toutes les parties du bateau sont sur une case
    //Return false si il y a une partie qui n'est pas sur une case
    public bool CheckShipParts()
    {
        bool returnFunction = true;
        isChecked.Clear();   
        for (int i = 0; i < ship.shipSize; i++)
        {
            if (transform.GetChild(i).GetComponent<CubeDetection>().caseObject == null)
            {
                isChecked.Add(false);
            }
            else
            {
                isChecked.Add(true);
            }
        }

        foreach (var boolean in isChecked)
        {
            if (boolean == false)
            {
                returnFunction = false;
            }
        }
        return returnFunction;
    }
}