﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;
using UnityEngine.Experimental.Animations;

public class CubeDetection : MonoBehaviour
{

    public GameObject caseObject;
    public bool isNearOtherShip;
    public LayerMask layer;

    public float rayDistance;
    
    public bool isTouched;
    // Start is called before the first frame update
    void Start()
    {
        //Debug.Log(transform.parent.GetComponent<ShipManagerVM>().name);
        //NearOtherShip();
    }

    // Update is called once per frame
    void Update()
    {

        BoxCollider2D boxCollider = GetComponent<BoxCollider2D>();
        Debug.DrawRay(boxCollider.bounds.center + new Vector3(0,0.55f) + new Vector3(boxCollider.bounds.extents.x+0.40f,0),Vector2.down * (boxCollider.bounds.extents.y + rayDistance),Color.yellow);
        Debug.DrawRay(boxCollider.bounds.center + new Vector3(0,0.55f) - new Vector3(boxCollider.bounds.extents.x+0.40f,0),Vector2.down * (boxCollider.bounds.extents.y+ rayDistance),Color.red);
        Debug.DrawRay(boxCollider.bounds.center - new Vector3(0.3f,0) - new Vector3(boxCollider.bounds.extents.x,boxCollider.bounds.extents.y+0.40f),Vector2.right * (boxCollider.bounds.extents.x + rayDistance),Color.blue);
        Debug.DrawRay(boxCollider.bounds.center - new Vector3(0.3f,0) - new Vector3(boxCollider.bounds.extents.x,boxCollider.bounds.extents.y-0.80f),Vector2.right * (boxCollider.bounds.extents.x + rayDistance),Color.blue);

    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.CompareTag("GridCaseForShip"))
        {
            caseObject = other.gameObject;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("GridCaseForShip") && transform.parent.GetComponent<ShipManagerVM>().canBeMoved)
        {
            //Debug.Log("Dettach " + other.name);
            caseObject = null;
        }
    }
    
    //Test pour savoir si il y a des cases d'un autre bateau a coté de la case
/*
    public bool NearOtherShip()
    {
        bool returnFunction = false;
        if (hitShipPart.transform.parent.name != transform.parent.name)
        {
            Debug.Log("Near boats !");
            returnFunction = true;
        }
        
        return returnFunction;
    }*/

}
