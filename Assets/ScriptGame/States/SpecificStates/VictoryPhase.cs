﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VictoryPhase : State
{
    // Start is called before the first frame update

    public VictoryPhase(GameplayManager gameplayManager) : base(gameplayManager)
    {
        
    }
    
    
    public override IEnumerator VictoryPlayer1()
    {
        Debug.Log("VictoryP1");
        GameplayManager.replayButton.SetActive(true);
        GameplayManager.textInfo.text = "Victoire du joueur 1 !";
        Time.timeScale = 0;
        //TODO Faire un bouton REJOUER -> Boucle de gameplay
        yield break;
    }
    
    public override IEnumerator VictoryPlayer2()
    {
        Debug.Log("VictoryP2");
        GameplayManager.replayButton.SetActive(true);
        GameplayManager.textInfo.text = "Victoire du joueur 2 !";
        Time.timeScale = 0;
        //TODO Faire un bouton REJOUER -> Boucle de gameplay
        yield break;
    }
}
