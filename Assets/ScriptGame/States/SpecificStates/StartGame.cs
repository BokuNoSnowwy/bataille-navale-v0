﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartGame : State
{
    public StartGame(GameplayManager gameplayManager) : base(gameplayManager)
    {
        
    }

    public override IEnumerator ShipPlacementPlayer1()
    {
        GameplayManager.textInfo.text = "Joueur 1, placez vos bateaux sur la grille gauche puis clickez sur \"Valider\"";
        yield break;
    }


    public override IEnumerator ShipPlacementPlayer2()
    {
        GameplayManager.textInfo.text = "Joueur 2, placez vos bateaux sur la grille gauche puis clickez sur \"Valider\"";
        GameplayManager.Player1Objects.SetActive(false);
        GameplayManager.Player2Objects.SetActive(true);
        
        GameplayManager.Player1ValidateButton.SetActive(false);
        GameplayManager.Player2ValidateButton.SetActive(true);
        
      

        GameplayManager.SetState(new BattlePhase(GameplayManager));
        
        yield break;
    }

    /*
    public override IEnumerator ShipAddPlayer1(GameObject ship)
    {
        GameplayManager.listOfShipP1.Add(ship);
        yield break;
    }
    
    
    public override IEnumerator ShipAddPlayer2(GameObject ship)
    {
        GameplayManager.listOfShipP2.Add(ship);
        yield break;
    }
    */
}


