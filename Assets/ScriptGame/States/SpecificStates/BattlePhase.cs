﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattlePhase : State
{
    public BattlePhase(GameplayManager gameplayManager) : base(gameplayManager)
    {
        
    }

    public override IEnumerator PutShipsOnBattlePhase()
    {
        foreach (var ship in GameplayManager.listOfShipP2)
        {
            ship.GetComponent<ShipManagerVM>().BattlePhase();
        }
        
        foreach (var ship in GameplayManager.listOfShipP1)
        {
            ship.GetComponent<ShipManagerVM>().BattlePhase();
        }

        GameplayManager.StartCoroutinePlayer1AttackTurn();
        yield break;
    }

    public override IEnumerator AttackPlayer1()
    {
        GameplayManager.player1CanAttack = true;
        Debug.Log("AttackPlayer1");
        GameplayManager.textInfo.text = "Joueur 1, Choisissez sur la grille de droite un endroit à attaquer";
        GameplayManager.DisplayElementP1();

        foreach (var ship in GameplayManager.listOfShipP1)
        {
            ship.GetComponent<ShipManagerVM>().BattlePhase();
        }

        yield break;
    }

    public override IEnumerator AttackPlayer2()
    {
        Debug.Log("AttackPlayer2");
        GameplayManager.textInfo.text = "Joueur 2, Choisissez sur la grille de droite un endroit à attaquer";
        GameplayManager.DisplayElementP2();
        GameplayManager.player1CanAttack = true;
        GameplayManager.player2CanAttack = true;
        
        
        foreach (var ship in GameplayManager.listOfShipP2)
        {
            ship.GetComponent<ShipManagerVM>().BattlePhase();
        }
        yield break;
    }

    public override IEnumerator DisplayAttackPlayer1(GameObject casePose)
    {
        //Debug.Log("Name of case " + casePose.name);
        //Debug.Log("Position case pose "+  casePose.transform.position);

        double casePosX = casePose.transform.localPosition.x;
        double casePosY = casePose.transform.localPosition.y;

        int touchedShip = 0;
        
        foreach (var ship in GameplayManager.listOfShipP2)
        {
            foreach (var aCase in ship.GetComponent<ShipManagerVM>().listCase)
            {
                //Debug.Log(aCase.name);
                //Comparaison des valeurs arrondies
                if (CompareCoordinate(casePosX,casePosY,aCase.GetComponent<CubeDetection>().caseObject.transform.localPosition.x,aCase.GetComponent<CubeDetection>().caseObject.transform.localPosition.y))
                {
                    touchedShip++;
                    HitShip(casePose,aCase,true);
                }
                else
                {
                    HitShip(casePose,aCase,false);
                }
            }
        }
        
        //GameplayManager.textInfo.text = "J1 Vous avez raté votre coup";
        GameplayManager.player1CanAttack = false;
        
        if (touchedShip > 0)
        {
            //Si le joueur touche un bateau, met la case en vert
            casePose.GetComponent<SpriteRenderer>().color = Color.green;
            GameplayManager.textInfo.text = "J1 Vous avez touché ";
        }
        else
        {
            //Si le joueur n'a pas touché le bateau, met la case en rouge
            casePose.GetComponent<SpriteRenderer>().color = Color.red;
            GameplayManager.textInfo.text = "J1 Vous avez raté votre coup";
        }
        
        yield return new WaitForSeconds(2f);
        //Check Victory du joueur
        GameplayManager.CheckVictory();
        
        if (touchedShip > 0)
        {
            GameplayManager.StartCoroutinePlayer1AttackTurn();
        }
        else
        {
            GameplayManager.StartCoroutinePlayer2AttackTurn();
        }


    }

    public override IEnumerator DisplayAttackPlayer2(GameObject casePose)
    {
        //Debug.Log("Name of case " + casePose.name);
        //Debug.Log("Position case pose "+  casePose.transform.position);

        double casePosX = casePose.transform.localPosition.x;
        double casePosY = casePose.transform.localPosition.y;

        int touchedShip = 0;

        foreach (var ship in GameplayManager.listOfShipP1)
        {
            foreach (var aCase in ship.GetComponent<ShipManagerVM>().listCase)
            {
                //Comparaison des valeurs arrondies
                if (CompareCoordinate(casePosX,casePosY,aCase.GetComponent<CubeDetection>().caseObject.transform.localPosition.x,aCase.GetComponent<CubeDetection>().caseObject.transform.localPosition.y))
                {
                    HitShip(casePose,aCase,true);
                    touchedShip++;
                }
                else
                {
                    HitShip(casePose,aCase,false);
                }
            }
        }
        GameplayManager.player2CanAttack = false;
        
        if (touchedShip > 0)
        {
            //Si le joueur touche un bateau, met la case en vert
           casePose.GetComponent<SpriteRenderer>().color = Color.green;
           GameplayManager.textInfo.text = "J2 Vous avez touché ";
        }
        else
        {
            //Si le joueur n'a pas touché un bateau, met la case en rouge
            casePose.GetComponent<SpriteRenderer>().color = Color.red;
            GameplayManager.textInfo.text = "J2 Vous avez raté votre coup";
        }
        
        yield return new WaitForSeconds(2f);
        
        //Check Victory du joueur
        GameplayManager.CheckVictory();

        if (touchedShip > 0)
        {
            GameplayManager.StartCoroutinePlayer2AttackTurn();
        }
        else
        {
            GameplayManager.StartCoroutinePlayer1AttackTurn();
        }
    }

    public void HitShip(GameObject caseGridRight, GameObject shipPart, bool hit)
    {
        //Debug.Log(shipPart.name);
        //Debug.Log(caseGridRight.name);
        //Si la bateau est touché
        if (hit)
        {
            //Rend la case du bateau rouge et active un boolean le rendant toucher
            shipPart.GetComponent<CubeDetection>().isTouched = true;
            shipPart.GetComponent<SpriteRenderer>().color = Color.red;
            //Vérifie si le bateau a encore des parties après le coup réussi
            shipPart.transform.parent.GetComponent<ShipManagerVM>().CheckRemainingCase();
        }
        caseGridRight.GetComponent<GridCase>().isUsed = true;
    }

    public bool CompareCoordinate(double posXCaseRight, double posYCaseRight, double posXCaseLeft, double posYCaseLeft)
    {
        bool returnFunction = posXCaseRight == posXCaseLeft && posYCaseLeft == posYCaseRight;
        return returnFunction;
    }

}
