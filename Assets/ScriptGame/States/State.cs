﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class State
{

   protected GameplayManager GameplayManager;

   public State(GameplayManager gameplayManager)
   {
      GameplayManager = gameplayManager;
   }

   public virtual IEnumerator ShipPlacementPlayer1()
   {
      yield break;
   }

   public virtual IEnumerator ShipPlacementPlayer2()
   {
      yield break;
   }

   /*
   public virtual IEnumerator ShipAddPlayer1(GameObject ship)
   {
      yield break;
   }
   
   
   public virtual IEnumerator ShipAddPlayer2(GameObject ship)
   {
      yield break;
   }
   */

   public virtual IEnumerator AttackPlayer1()
   {
      yield break;
   }
   
   public virtual IEnumerator AttackPlayer2()
   {
      yield break;
   }

   public virtual IEnumerator PutShipsOnBattlePhase()
   {
      yield break;
   }
   
   public virtual IEnumerator DisplayAttackPlayer1(GameObject casePose)
   {
      yield break;
   }
   
   public virtual IEnumerator DisplayAttackPlayer2(GameObject casePose)
   {
      yield break;
   }
   
   public virtual IEnumerator IsTheCaseOccupiedPlayer1(Vector2 positionCase)
   {
        yield break;
   }
   
   public virtual IEnumerator IsTheCaseOccupiedPlayer2(Vector2 positionCase)
   {
      yield break;
   }
   
   public virtual IEnumerator VictoryPlayer1()
   {
      yield break;
   }

   public virtual IEnumerator VictoryPlayer2()
   {
      yield break;
   }


}
