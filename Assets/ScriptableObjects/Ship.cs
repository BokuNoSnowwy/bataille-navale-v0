﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "ShipObject", menuName = "Scripts/ScriptableObjects")]
public class Ship : ScriptableObject
{
    public string shipName;
    public int shipSize;
    public int Player;
    public GameObject shipCube;
}
