﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ShipTest : MonoBehaviour
{
    public Transform yourShip;
    public Ship ship;
    public float distance = 10f;
    public float intRotate = 90;
    public bool isSelected;
    public CubeDetection cubeDetection;
    public void Start()
    {
        for (int i = 0; i < ship.shipSize; i++)
        {
            Instantiate(ship.shipCube,new Vector2(yourShip.position.x+0.55f * i,yourShip.position.y),Quaternion.identity,yourShip);
        }
    }

    public void OnMouseDrag()
    {
            isSelected = true;
            Vector3 mousePos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, distance);
            Vector3 objPos = Camera.main.ScreenToWorldPoint(mousePos);
            transform.position = objPos;
    }

    public void OnMouseExit()
    {
        isSelected = false;
    }

    public void Update()
    {
        if (Input.GetMouseButtonDown(1) && isSelected)
        {
            transform.Rotate(new Vector3(0,0,1),intRotate);
            Debug.Log("test");
        }

        cubeDetection.caseObject.transform.position = this.gameObject.transform.position;
    }
}
